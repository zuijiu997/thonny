=======
Credits
=======

Thonny is thankful to:

Its home
--------
Thonny was born in University of Tartu (https://www.ut.ee), Institute of Computer Science (https://www.cs.ut.ee) and the main development is being done here.

Python
------
No comments :)

Libraries
--------------
* jedi (http://jedi.readthedocs.io) is used for code completion, go to definition, etc.
* certifi (https://pypi.python.org/pypi/certifi) provides SSL certs for bundled Python in Linux and Mac.
* distro (https://pypi.python.org/pypi/distro) is optionally used for detecting Linux version in About dialog. 

Source contributors and frequent bug-reporters
----------------------------------------------
* Aivar Annamaa
* Rene Lehtma
* Filip Schouwenaars
* Sven (s_v_e_n)
* Taavi Ilp
* Xin Rong

Please let us know if we're forgotten to add your name to this list! Also, let us know if you want to remove your name.